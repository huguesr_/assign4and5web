"use strict";
document.addEventListener("DOMContentLoaded", () => {
    (function() {
        const EpicApplication = {
            imgType: "natural",
            imagesArray: [],
            imageTypes: ["natural", "enhanced", "aerosol", "cloud"],
            DateCache: {},
            imageCache: {}
        };
        EpicApplication.imageTypes.forEach((type) => EpicApplication.DateCache[type] = null);
        EpicApplication.imageTypes.forEach((type) => EpicApplication.imageCache[type] = new Map());

        const menu = document.querySelector('#image-menu');
        menu.addEventListener("click", (e) => {
            let currLi = e.target.closest('li');
            const imgDisplayed = document.querySelector('#earth-image');
            const dateChosen = EpicApplication.imagesArray[currLi.getAttribute('data-image-list-index')].date.split(' ')[0].split('-').join('/');
            let imgLink = `https://epic.gsfc.nasa.gov/archive/${EpicApplication.imgType}/${dateChosen}/jpg/${EpicApplication.imagesArray[currLi.getAttribute('data-image-list-index')].image}.jpg`
            imgDisplayed.src = imgLink;
            document.querySelector('#earth-image-date').textContent = EpicApplication.imagesArray[currLi.getAttribute('data-image-list-index')].date;
            document.querySelector('#earth-image-title').textContent = EpicApplication.imagesArray[currLi.getAttribute('data-image-list-index')].caption;
            // console.log(imagesArray[currLi.getAttribute('data-image-list-index')]);
        });

        getDates();
        document.querySelector('#type').addEventListener("change", getDates);
        document.querySelector('#request_form').addEventListener('submit', (e) => {
            EpicApplication.imgType = document.querySelector('#type').value;
            const dateChosen = document.querySelector('#date').value;
            e.preventDefault();
            menu.innerHTML = '';
            if(EpicApplication.imageCache[EpicApplication.imgType].has(dateChosen)){
                imgSetting(EpicApplication.imageCache[EpicApplication.imgType].get(dateChosen));
            }
            else {
                fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.imgType}/date/${dateChosen}`).then((r) => r.json()).then((r) => {
                    EpicApplication.imagesArray = r;
                    EpicApplication.imageCache[EpicApplication.imgType].set(dateChosen, EpicApplication.imagesArray);
                    imgSetting(EpicApplication.imagesArray);
                });
            }
        });

        function getDates() {
            let imgType = document.querySelector('#type').value;
            const dateInput = document.querySelector('#date');
            if (EpicApplication.DateCache[imgType] == undefined){
                fetch(`https://epic.gsfc.nasa.gov/api/${imgType}/all`).then((r) => r.json()).then((r) =>{
                    r.sort(function(a, b){return b.date-a.date});
                    let dateString = r[0].date;
                    dateInput.max = dateString;
                    EpicApplication.DateCache[imgType] = dateString;
                });
            }
            else {
                dateInput.max = EpicApplication.DateCache[imgType];
            }
        };

        function imgSetting(imgsArray){
            if (imgsArray.length === 0){
                const err = document.querySelector('#image-menu-item').content.cloneNode(true).children[0];
                err.innerText = "No images for selected date/type";
                menu.appendChild(err);
            }
            else {
                imgsArray.forEach((imgE, i) => {
                    const element = document.querySelector('#image-menu-item').content.cloneNode(true).children[0];
                    element.setAttribute("data-image-list-index", i);
                    element.querySelector('.image-list-title').innerText = imgE.date;
                    menu.appendChild(element);
                });
            }
        }
    })();
});